<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', function () {
    return view('table');
});

Route::get('/add', function () {
    return view('add');
});


Route::get('/home', function () {
    return view('home');
});


Route::resource('/topic', 'TopicController');
Auth::routes();

Route::get('/home', 'TopicController@index')->name('home');

