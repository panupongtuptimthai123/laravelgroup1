@extends('layouts.app')

@section('content')

<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<div class="container">
    <div class="card">
        <div class="card-body">
                <form action="{{url('topic')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>ชื่อกระทู้</label>
                            <input type="text" class="form-control" name="post_name">
                        </div>
                        <div class="form-group">
                            <label>ข้อความ</label>
                            <textarea name="post_text" id="post_text" class="ckeditor" cols="69" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            
                            <input type="text" class="form-control" name="id" value="{{ Auth::user()->id }}"  hidden>
                        </div>
                        <button type="submit" class="btn btn-success">ตั้งกระทู้</button> 
                        
                        
                        <br>
                        <br>
                        @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        
                        </form>

                        
                                <a href="{{url('home')}}">
                                <button class="btn btn-primary" type="submit">ยกเลิก</button>
                                </a>
                        
                            
        </div>
    </div>
</div>

@endsection