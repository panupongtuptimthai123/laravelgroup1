@extends('layouts.app')
@section('title','HOME')
@section('content')

@csrf

<div class="container">
    <div class="row justify-content-center">
       
           

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
<div class="form-group">
        <a href="{{url('topic/create')}}">
        <button class="btn btn-primary" type="submit">เพิ่มกระทู้</button>
        </a>
</div>

  
<div class="card">
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
                <th>ID</th>
                <th>กระทู้</th>
                <th>ผู้โพสต์</th>
                <th>วันเวลาโพสต์</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($post2 as $p)
                    <tr>
                    <td>{{$p->id_post}}</td>
                    <td>{{$p->post_name}}</td>
                    <td>{{$p->id()->name}}</td>
                    <td>{{$p->post_time}}</td>
                  
                    {{-- <td>{{date('Y-mm-dd H:i:s',strtotime($p -> created_at))}}</td> --}}
                    {{-- <td>{{date('Y-mm-dd H:i:s',strtotime($p -> updated_at))}}</td> --}}
                    <td>
                        <div class="form-inline">
                            <a href="{{url('topic/' .$p->id.'/edit')}}">
                                <button class="btn btn-primary" type="button">EDIT</button>
                            </a>
                            &nbsp;
                            <form action="{{url('topic/' .$p->id)}}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-warning" type="submit">Delete</button>
                        </div>
                    </form>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

                          
                         

      
            </div>
        </div>
    </div>
</div>
@endsection
